-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA256

Format: 3.0 (native)
Source: ctags
Binary: ctags
Architecture: all
Version: 1.0.2
Maintainer: Matt Jenkins <matt@majenko.co.uk>
Standards-Version: 3.9.4
Build-Depends: debhelper (>= 8.0.0)
Package-List:
 ctags deb tools required arch=all
Checksums-Sha1:
 de5995d281c88c7b57853eef73ada8266d1198f4 409600 ctags_1.0.2.tar.xz
Checksums-Sha256:
 6af928d42fe3550cc8097386479dd8b2a5874ee8fe17e36ddf434cb210c73b3f 409600 ctags_1.0.2.tar.xz
Files:
 1bf21f29ee3f4119cb600409afae71bd 409600 ctags_1.0.2.tar.xz
Group: Core

-----BEGIN PGP SIGNATURE-----

iQEwBAEBCAAaBQJa9r0AExxtYXR0QG1hamVua28uY28udWsACgkQ5ZaBXoyLZ34b
Nwf/WWWiOiQiozgy3Zj/3Oclzg/7lKWVYAUR8FOUT9FjpvMtNeSwPIH0aVo+Pagp
a+lFpkJ32FAm772LBA689eRzzH0z3gTjNHV1pLg5HULw4nnMzgY4xPwdP55UGPdX
8RYs+81/zMoRH++cNdDJYv2alAOZHgUt22TNdl28IrOEyNnjxZDi9qUZKbEP+rTp
0b301OYYhA1Q4ppW5ls9E7Wea5csQfDoG5dyi+ydUl1AhYXgvNXIsmgzDeColoRU
q2WjnQqwvcAmFHbnRla81YzLCP+odrPy0HcNauTG8llMH+QuGaXm97JszNE6iAAc
nHLyP6Oyk1bxlg/v7NdKAc+OyQ==
=Uv5S
-----END PGP SIGNATURE-----
