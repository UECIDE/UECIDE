-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA256

Format: 3.0 (native)
Source: core-varcmd
Binary: core-varcmd
Architecture: all
Version: 1.0.6
Maintainer: Matt Jenkins <matt@majenko.co.uk>
Homepage: http://uecide.org
Standards-Version: 3.9.4
Build-Depends: debhelper (>= 8.0.0)
Package-List:
 core-varcmd deb extra extra arch=all
Checksums-Sha1:
 cb0d0c5b72eb5aaf5d15b0c9ba20ef76024bf226 6124 core-varcmd_1.0.6.tar.xz
Checksums-Sha256:
 77e3baf361333e8db9eb39c71ee77ce512a8fbf76c8f8c12a4867278fd59bd04 6124 core-varcmd_1.0.6.tar.xz
Files:
 27e8bd7e571b3193a7e7d4b63cfcb8bd 6124 core-varcmd_1.0.6.tar.xz
Family: pic32
Group: System
Subgroup: Scriptlets

-----BEGIN PGP SIGNATURE-----

iQEwBAEBCAAaBQJa076LExxtYXR0QG1hamVua28uY28udWsACgkQ5ZaBXoyLZ36H
Jwf/VcWkj6tz6zGSbeTczBPBWbueCQH18rnM0myw+I2fTpchST7QTx2mPB1IKrQf
TsEGcQ6s/DLlfkgllV7hSrQEQe6agI6Tzm0NPqu9P2S50gJvZcoznEbDkzqA7pLM
Qm5w/jO6z8BcmY+qEaBQ5EyMtFPFkgcEoVm9/bOUSCKq3PI34/iHhc2YScd5qrJF
pmWHPdu7GAdID0PfYpInp5Pe83zC+er6nvmC+wTQTQTh6qHR+ul3y2ub1NHjuVeV
CgsNL+W+iZCEGhnICcXdpONCucvDogpis+AHo4mKIkio16e7QUjookyYQBdcGQ4J
BO5AGWARAZEVYWc8lXiMTE2Lsg==
=2U+T
-----END PGP SIGNATURE-----
