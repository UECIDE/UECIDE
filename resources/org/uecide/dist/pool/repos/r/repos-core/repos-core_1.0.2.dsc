-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA256

Format: 3.0 (native)
Source: repos-core
Binary: repos-master, repos-nightly
Architecture: all
Version: 1.0.2
Maintainer: Matt Jenkins <matt@majenko.co.uk>
Standards-Version: 3.9.4
Build-Depends: debhelper (>= 8.0.0)
Package-List:
 repos-master deb repos extra arch=all
 repos-nightly deb repos extra arch=all
Checksums-Sha1:
 75fea3b1201d21e262818429915640efa802e58f 1072 repos-core_1.0.2.tar.xz
Checksums-Sha256:
 bf62fbaa40a5f62dd180bd4ed09b5589fb4b6b44b59844cf6d5afd46346b5562 1072 repos-core_1.0.2.tar.xz
Files:
 cc088b6714e05168fa6ba243edd7cc7e 1072 repos-core_1.0.2.tar.xz
Group: Core

-----BEGIN PGP SIGNATURE-----

iQEwBAEBCAAaBQJa+B9vExxtYXR0QG1hamVua28uY28udWsACgkQ5ZaBXoyLZ344
FwgAnb4n7ZEXi0Sx2TwmMZVMB+zOtlp89MOlSupiCXMiIgFiggt+MBs4g1f97z/m
1CVxfLjLN7MWiIIRPQuv3tfEiZY+xNzaUAtuGFv5VU7ygKwT5F5ShFG/FO9eGI6D
ii6ec2cS/gylL06f06Bf9EIZKALIzsfISgjYeeERVT0HkMXa2scqEKIRtm1q9AOK
3OYQYk8J//ngqTdZmDHzJi9COBt9POAkJMs+0+WZ8JDPos5c35WkxAMCUwQTEtym
PjvWWxGtM5hqj9QQ46mFwMY3Srg9x1yrTZtZzkMVoDXDc7fwUOlyOGHNx8IWkuDM
CY7fJnUWb4gwCAP4zhX3rDom0w==
=LVoc
-----END PGP SIGNATURE-----
